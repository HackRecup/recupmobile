import { IonImg, IonText, IonRouterLink } from "@ionic/react";
import React from "react";
import styled from "styled-components";

const Text = styled(IonText)`
    position: absolute;
    bottom: 0;
    width: 150px;
  height: 140px;
  display: flex;
  justify-content: center;
  padding: 12%;
  
  font-family: Montserrat;
  font-size: 26px;
  font-weight: bold;
  font-stretch: normal;
  font-style: normal;
  line-height: normal;
  letter-spacing: normal;
  text-align: right;
  color: #ffffff;
  z-index: 1;
  `;

const Image = styled(IonImg)`
    position: absolute;
    bottom: 0;
    width: 150px;
  height: 140px;
    
`;


const PickupButton: React.FC = () => {

    return (<IonRouterLink href={"/home/PickupCup"}><Text>
        ReCup
        pickup
    </Text><Image src={"assets/buttons/rectangle-copy-2.png"}>

    </Image></IonRouterLink>);
};

export default PickupButton;