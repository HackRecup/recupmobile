import { IonImg, IonMenuButton } from '@ionic/react';
import React from 'react';
import styled from "styled-components";

const Image = styled(IonImg)`
  max-height: 35vh;
  width: 100%;
  overflow: hidden;
    position: absolute;
    top: 0;
    z-index: 1;
`;


const Menu = styled(IonMenuButton)`
  position: absolute;
  right: 10px;
  top: 10px;
  width: 70px;
  height: 70px;
  color: transparent;
  background-color: transparent;
    z-index: 2;
`;

const Header: React.FC = () => {

    return (
                <div>
                    <Image src="/assets/headerPRO@1x.png"/>
                    <Menu />
                </div>
    );
};

export default Header;
