import { useIonViewDidEnter} from "@ionic/react";
import axios from "axios";
import React from "react";
import styled from "styled-components";

const MapDiv = styled.div`
  width: 100%;
  height: 100%;
`;


function createMapScript(){
    var head = document.getElementsByTagName('head').item(0);
    var script = document.createElement('script');
    script.setAttribute('src', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyDET2TtRlS4CUna8-XBE6r0o68vRhZ2i1g&callback=initMap');
    head && head.appendChild(script);

}

// @ts-ignore
const addMarker = (position) => new window.google.maps.Marker({position, map: window.recupMap});

// Initialize and add the map
const initMap = function () {
    let mapElement = document.getElementById('map');

    var kenshooTlv = {lat: 32.109813, lng: 34.84097};

    // @ts-ignore
    window.recupMap = new window.google.maps.Map(
        mapElement, {zoom: 15, center: kenshooTlv});


    // @ts-ignore
    axios.get("https://recup-server.herokuapp.com/businesses/").then(({ data }) => {
        // @ts-ignore
        data.map((company: { long: any; }) => addMarker({ lng: Number(company.longitude), lat: Number(company.latitude)}));
    });
};
// @ts-ignore
window.initMap = initMap;

const Map: React.FC = () => {
    useIonViewDidEnter(() => {
        createMapScript();
    });

    return (<MapDiv id={"map"} />);
};

export default Map;