// import { getEnvironment } from "../utils/environment_utils";
import axios from "axios";

const getBusinessDataPath = businessId => { return "http://www.mocky.io/v2/5e1c69cc3200007000228581"
  // return getEnvironment() === "dev"
  //   ? "http://www.mocky.io/v2/5e1b0e9a3100007b004f3235"
  //   : "/api/audit_group/" + auditGroupId;
};

export const getBusinessData = businessId => {
    return axios
        .get(getBusinessDataPath(businessId))
        .then(response => {
            return response.data;
        })
        .catch(error => {
            console.log(`Error getting business data from business id=${businessId} ` + error.message);
        });
};
