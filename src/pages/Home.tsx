import { IonContent, IonPage } from '@ionic/react';
import React from 'react';
// @ts-ignore
import MapView from "../components/MapView";
import PickupButton from "../components/PickupButton";
import ReturnButton from "../components/ReturnButton";
import Header from "../components/Header";

const HomePage: React.FC = () => {

  return (
    <IonPage>
      <Header />
      <IonContent>
        <MapView />
        <PickupButton />
        <ReturnButton />
      </IonContent>
    </IonPage>
  );
};

export default HomePage;
