import {
    IonContent,
    IonPage,
    useIonViewDidEnter
} from '@ionic/react';
import React, {useState} from 'react';
import './List.css'
import Header from "../components/Header";
import {getBusinessData} from "../integration/business_data_fetch_request";

const ListPage: React.FC = () => {
    const [businessName,setBusinessName] = useState("");
    const [availableCups,setAvailableCups] = useState(0);
    const [reservedCups,setReservedCups] = useState(0);
    const [isScanClick,setIsScanClick] = useState(false);
    useIonViewDidEnter(() => {
        getBusinessData(1)
            .then(businessData => {
                businessData && setBusinessName(businessData.businessName);
                businessData && setAvailableCups(businessData.availableCups);
                businessData && setReservedCups(businessData.reservedCups);
            });
    });

  return (
    <IonPage>
     <Header/>
      <IonContent>
          {isScanClick && <BarcodeScanCard/>}
          {!isScanClick && <BusinessCard {...{setIsScanClick,businessName,availableCups,reservedCups}} />}
      </IonContent>
    </IonPage>
  );
};

const BusinessCard = (data:any) => {

    return <div className="business-card">
        <img className="top-bar-image" src="/assets/headerPRO@1x.png" alt=""/>
        <div className="business-name-title">{data.businessName}</div>
        <div className="available-cups">
            <div className="recup-available-container">
                <div className="available-cups-number">{data.availableCups}</div>
                <div className="available-cups-title">ReCups available</div>
            </div>
            <div className="update-quantities">
                <div className="update-quantities-title">Update quantities</div>
            </div>
            <div className="reserved-cups">
                <div className="reserved-cups-number">{data.reservedCups}</div>
                <div className="reserved-cups-title">ReCups reserved</div>
            </div>
        </div>
        <div className="scan-barcode">
            <div onClick={() => data.setIsScanClick(true)} className="scan-barcode-title">
                Scan client barcode
            </div>
        </div>
    </div>
};

const BarcodeScanCard = () => {
    return <div className="scan-card">
        <img className="scan-image" src="/assets/scan.png" alt=""/>
    </div>
};

export default ListPage;
