import {
    IonPage,
    IonImg, IonRouterLink
} from '@ionic/react';
import React from 'react';
import styled from "styled-components";
import Header from "../components/Header";
// @ts-ignore

const Image = styled(IonImg)`
    position: absolute;
    top: 70px;
`;

const PickupCup: React.FC = () => {

    return (
        <IonPage>
            <Header />
            <IonRouterLink href={"/home"}>
            <Image src={"assets/pages/flach-pickup.png"} />
            </IonRouterLink>
        </IonPage>
    );
};

export default PickupCup;
